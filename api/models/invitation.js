'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema; 

var InvitationSchema = Schema({
	email: String,
	token: String,
    email: String,
    role: {type: Schema.ObjectId, ref: 'Role'},
	status: String
});


module.exports = mongoose.model('Invitation', InvitationSchema);
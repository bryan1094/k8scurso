'use strict'

var mongoose = require('mongoose') 
var Schema = mongoose.Schema; 


var UserSchema = Schema({
	name: String,
	surname: String,
	email: String,
	password: String,
	role: {type: Schema.ObjectId, ref: 'Role'}, 
	reset_password: Boolean,
	token: String,
	img:String,
	status: String
});

module.exports = mongoose.model('User', UserSchema);
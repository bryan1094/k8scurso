'use strict'

var Object = require ('../models/role');


function getRoles(req, res){
    
    Object.find({status: 'ACTIVE'}).exec((err, roles) =>{
        if(err) return res.status(500).send({message:'Error en el servidor'});

        if(!roles) return res.status(404).send({message: 'No hay registros para mostrar'});

        return res.status(200).send({data: roles});
    });
}

function saveRole(req, res){
	var params = req.body;

	var role = new Object();

	if(params.name){

		role.name = params.name;
		role.description = params.description;
		role.status = 'ACTIVE';
		
		Object.findOne({ $or: [
				 {name: role.name}
		]}).exec((err, roles)=> {
			if(err) return res.status(500).send({message: 'Error en la peticion'});
			if(roles) return res.status(200).send({message: 'El registro ya existe'});
			else{
				
				role.save((err, roleStored) => {
					if(err) return res.status(500).send({message: 'Error al guardar registro'});
					if(roleStored) return res.status(200).send({data: roleStored});
					else return res.status(404).send({message: 'No se guardo el registro'});
						
				});
			}
		});
	}else{
		res.status(200).send({
			message: 'Completar campos obligatorios!!'
		});
	}

}

function updateRole(req, res){
	var obsjectId = req.params.id;
	var update = req.body;

	Object.find({ $or: [
		{name: update.name}
	]}).exec((err, roles) => {

		var role_isset = false;
		roles.forEach((admin) => { 
			if(admin && admin._id != obsjectId) role_isset = true;
		});

		if(role_isset) return res.status(404).send({message: 'Los datos ya estan en uso'});
	
		Object.findByIdAndUpdate(obsjectId, update, {new:true}, (err, roleUpdated) => {
			if(err) return res.status(500).send({message: 'Error en la peticion'});


			if(!roleUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});

			return res.status(200).send({admin: roleUpdated});
		});	
	});		
}

function deleteRole(req, res){
    var obsjectId = req.params.id;
     
    Object.findByIdAndUpdate(obsjectId, {status:'INACTIVE'}, {new:true}, (err, roleUpdated) => {
        if(err) return res.status(500).send({message: 'Error en la peticion'});
 
       if(!roleUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});
 
       return res.status(200).send({data: roleUpdated});
    });			
 }


function activeRole(req, res){
    var obsjectId = req.params.id;
     
    Object.findByIdAndUpdate(obsjectId, {status:'ACTIVE'}, {new:true}, (err, roleUpdated) => {
        if(err) return res.status(500).send({message: 'Error en la peticion'});
 
 
       if(!roleUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});
 
       return res.status(200).send({role: roleUpdated});
    });			
 }


module.exports = {
    getRoles,
    saveRole,
    updateRole,
    deleteRole,
    activeRole
    
}
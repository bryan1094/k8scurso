'use strict'
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs'); 
var path = require('path'); 

var User = require('../models/user');  
var Role = require('../models/role');  
var jwt = require('../services/jwt');  
const { param } = require('../routes/user');
const role = require('../models/role');
var randtoken = require('rand-token'); 
var nodemailer = require('nodemailer'); 
const user = require('../models/user');
const invitation = require('../models/invitation');


function initProject(req, res){
	User.find({status: 'ACTIVE'}).exec((err, users) =>{
        if(err) return res.status(500).send({message:'Error en el servidor'});

        if(!users.length > 0){
			var role = new Role();
			role.name = 'Administrador';
			role.description = 'Administrador';
			role.status = 'ACTIVE';
			
					
			role.save((err, roleStored) => {
				if(err) return res.status(500).send({message: 'Error al guardar registro'});
				if(roleStored){
					var user = new User();

					user.name = 'Administrador';
					user.surname = '.';
					user.email = 'k8s@admin.com';
					user.role = roleStored._id;
					user.reset_password = false;
					user.token = null
					user.image = null;
					user.status = 'ACTIVE';
					user.password = bcrypt.hash('123456', null, null,(err,hash)=>{
						user.password = hash;
					});
					user.save((err, userStored) => {
						if(err) return res.status(500).send({message: 'Error al guardar registro'});								
					});
				} 
				else return res.status(404).send({message: 'No se guardo el registro'});			
			});
		} 

        return res.status(200).send({users});
    });

}


function saveUser(req, res){
	var params = req.body;
	var user = new User();

	if(params.name && params.surname && 
	   params.role && params.email && params.password){

		user.name = params.name;
		user.surname = params.surname;
		user.email = params.email;
		user.role = params.role;
		user.reset_password = false;
		user.token = null
		user.image = null;
		user.status = 'ACTIVE';


		User.findOne({ $or: [
			{email: user.email.toLowerCase()}
		]}).exec((err, users)=> {
			if(err) return res.status(500).send({message: 'Error en consultar elemento'});
			if(users) return res.status(200).send({message: 'El registro ya existe'});
			else{
				user.password = bcrypt.hash(params.password, null, null,(err,hash)=>{
				user.password = hash;
				});
				user.save((err, userStored) => {
					if(err) return res.status(500).send({message: 'Error al guardar registro'});
					if(userStored) return res.status(200).send({user: userStored});
					else return res.status(404).send({message: 'No se guardo el registro'});
						
				});
			}
		});
	}else{
		res.status(200).send({
			message: 'Completar campos obligatorios!!'
		});
	}

}


function getUser(req, res){
	var userId = req.params.id;

	User.findById(userId).populate('role').exec( (err, user) => {
		if(err) return res.status(500).send({message: 'Error en la peticion'});
		
		if(!user) return res.status(404).send({message: 'Usuario no existe'});
		
		user.password = undefined; 
		return res.status(200).send({user});
	});
}

function getUsers(req, res){

	User.find({status: 'ACTIVE'}).populate('role').exec((err, users) =>{
        if(err) return res.status(500).send({message:'Error en el servidor'});

        if(!users) return res.status(404).send({message: 'No hay registros para mostrar'});

        return res.status(200).send({users});
    });
}


function loginUser(req, res){
	var params = req.body;

	var email = params.email;
	var password = params.password;

	User.findOne({email: email}, (err, user) => {
		if (err) return res.status(500).send({message: 'Error en la peticion'});

		//if (user.status != 'ACTIVE') return res.status(500).send({message: 'Tu usuario a infringido nuestras normas de comportamiento, ponte en contacto con nuestros administradores.'});

		if(user){
			bcrypt.compare(password, user.password, (err, check) => {
				if(check){

					if(params.gettoken){
						//generar y devolver token 
						return res.status(200).send({
							token: jwt.createToken(user)   
						});

					}else{
						//devolver datos de usuario
						user.password = undefined; //eviatr que devuelva el pass
						return res.status(200).send({user})
					}

				}else {
					return res.status(404).send({message: 'El usuario no se pudo identificar'});
				}
			});
		}else {
			return res.status(404).send({message: 'El usuario no se pudo identificar!!'});
		}
	});
}

function updateUsers(req, res){
	var userId = req.params.id;
	var update = req.body;

	//borrar propiedad password
	delete update.password;

	// if(userId != req.user.sub){
	// 	return res.status(500).send({message: 'No tiene permiso para editar este regustro'});
	// }

	User.find({ $or: [
		{email: update.email.toLowerCase()}
	]}).exec((err, users) => {

		var user_isset = false;
		users.forEach((user) => { 
			if(user && user._id != userId) user_isset = true;
		});

		if(user_isset) return res.status(404).send({message: 'Los datos ya estan en uso'});
	
		User.findByIdAndUpdate(userId, update, {new:true}, (err, userUpdated) => {
			if(err) return res.status(500).send({message: 'Error en la peticion'});


			if(!userUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});

			return res.status(200).send({user: userUpdated});
		});	
	});		
}

function deleteUser(req, res){
     var obsjectId = req.params.id;
     
    User.findByIdAndUpdate(obsjectId, {status:'INACTIVE'}, {new:true}, (err, userUpdated) => {
    	if(err) return res.status(500).send({message: 'Error en la peticion'});
 
    	if(!userUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});
 
    	return res.status(200).send({user: userUpdated});
    });			
 }


function activeUser(req, res){
    var obsjectId = req.params.id;
     
    User.findByIdAndUpdate(obsjectId, {status:'ACTIVE'}, {new:true}, (err, userUpdated) => {
    	if(err) return res.status(500).send({message: 'Error en la peticion'});
 
 
    	if(!userUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});
 
		return res.status(200).send({user: userUpdated});
		
    });			
 }

function updatePasword(req, res){
	var obsjectId = req.params.id;
	var params = req.body;

	if(obsjectId != req.companie.sub){
		return res.status(500).send({message: 'No tiene permiso para edtar este registro'});
	}
		var update = bcrypt.hash(params.password, null, null,(err,hash)=>{
			update= hash;
			User.findByIdAndUpdate(obsjectId, {password: update}, {new:true}, (err, userUpdated) => {
				if(err) return res.status(500).send({message: 'Error en la peticion'});


				if(!userUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});

				return res.status(200).send({user: userUpdated});
			});	
		});	
}


function uploadImage(req, res){
	var userId = req.params.id;

    if(req.files){
    	var file_path = req.files.img.path;
    	console.log(file_path);

    	//separa patch de imagen
    	var file_split = file_path.split('/');
    	

    	//guardar nombre de atchivo
    	var file_name = file_split[2];
    	

    	//extencion de imagen
    	var ext_split = file_name.split('.');
    	
    	var file_ext = ext_split[1];
    	

    	if(file_ext == 'png' || file_ext == 'png' || file_ext == 'jpeg' ||  file_ext == 'jpg' || file_ext == 'gif'){
    		
    		User.findByIdAndUpdate(userId, {img: file_name}, {new:true}, (err, userUpdated) =>{
    			if(err) return res.status(500).send({message: 'Error en la peticion'});


				if(!userUpdated) return res.status(404).send({message: 'No se ah podido actualizar el registro'});

				return res.status(200).send({user: userUpdated});
    		});

    	}else{
    	
    		return removeFilesOfuploads(res, file_path, 'Extension no valida');

    	}

    }else{
    	return res.status(200).send({message: 'No se ha subido archivos'});
    }
}

function removeFilesOfuploads(res, file_path, message){
	fs.unlink(file_path, (err) => {
    	return res.status(200).send({message: message});
    });
}


function getImageFile(req, res){
	var image_file = req.params.imageFile;
	var path_file = './uploads/users/'+image_file;

	fs.exists(path_file, (exists) => {
		if(exists){
			res.sendFile(path.resolve(path_file));
		}else{
			res.status(200).send({message: 'Imagen no existe'});
		}
	});
}

function sendToken(req, res){
    var params = req.body;
   
    if(params.email){
        
		var token = randtoken.generate(16);
		
		User.find({ $or: [
			{email: params.email.toLowerCase()}
		]}).exec((err, users) => {
	
			if(users){
				User.findOneAndUpdate({email: params.email.toLowerCase()},{reset_password: true, token: token}, {new:true}, (err, userUpdated) => {
					console.log(userUpdated)
					if(userUpdated){

						return sendEmail(res, req, params.email, token);
				
					}
									 
					else return res.status(404).send({message: 'No se completo la operacion verifique los datos e intente de nuevo'});
				});
			}	
		});		
    }else {
        res.status(200).send({
            message: 'Completar campos obligatorios!!'
        });
    }   

}


function sendEmail (res, req, correo, token){
    
    
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'k8scurse@gmail.com',
            pass: 'ks82020@'
        }
	});
	
    // Definimos el email
    var mailOptions = {
        from: 'Curso k8s',
        to: correo,
        subject: 'Token',
        text: 'Has realizado una peticion de recuperacion de contraseña para actualizarla usa el siguiente Token: ' +  token 
    };
    // Enviamos el email
    transporter.sendMail(mailOptions, function(err, info){
        if (err){
            console.log(err);
            res.send(500, err.message);
        } else {
            res.status(200).send({data: req.body});
        }
    });
}

function updatePasswordForgot(req, res) {
    var update = req.body;
   
   
    if(update.token && update.password ){

		var passwordhash = bcrypt.hash(update.password, null, null,(err,hash)=>{
			passwordhash = hash;
		});


		User.findOne({token: update.token, reset_password: true}).exec( (err, user) => {
			if(user){

				User.findOneAndUpdate({token: update.token, reset_password: true},{password: passwordhash, reset_password: false}, {new:true}, (err, userUpdated) => {
				
					if(userUpdated) return res.status(200).send({data: userUpdated});
								
					else return res.status(404).send({message: 'No se actualizo el regustro, Verifique los datos he intente de nuevo'});
				});           
			}else {
				return res.status(404).send({message: 'Este registro no cuenta con los datos necesarios para poder actualizado'});
			}   
			
		});
             
    }else {
        res.status(200).send({
            message: 'Completar campos obligatorios!!'
        });
    }      
}



function singUp(req, res){
    
    var params = req.body
    var userStore = false

       
    invitation.findOne( { email: params.email, token: params.token, status: 'ACTIVE'}).exec( (err, object) => {
		if(object){
			console.log(object);
			var idInvitation = object._id;
			var emailResponse = object.email;
			var idUserRoleResponse = object.role;

			
			if(params.name && params.surname  && params.password){
        
				var user = new User();
				user.name = params.name;
				user.surname = params.surname;
				user.role =  idUserRoleResponse;
				user.email =  emailResponse;
				user.reset_password =  false;
				user.status = 'ACTIVE';
				
				User.findOne({email: params.email.toLowerCase()}).exec( (err, userResponse) => {
					if(userResponse){
						return res.status(200).send({message: 'La registro ya existe'})
					}else {

						user.password = bcrypt.hash(params.password, null, null,(err,hash)=>{
							user.password = hash;
						});
							
						user.save((err, userStored) => {
							if(err) return res.status(500).send({message: 'Error al guardar registro'});
		
							if(userStored) {
								invitationDone(res, req, idInvitation)
							}
							
							else return res.status(404).send({message: 'No se guardo el registro'});
								
						});                
					}
		
				});        
				 
			}else {
				res.status(200).send({
					message: 'Completar campos obligatorios!!'
				});
			} 
		}   

		if(!object) return res.status(404).send({message: 'El registro no existe'});

	});      
}


function invitationDone (res, req, invitationId){
    invitation.findOne({_id: invitationId}).exec( (err, invitationE) => {
		if(invitationE){
					
			invitation.findByIdAndUpdate(invitationId, {status: 'USED'}, {new:true}, (err, userUpdated) => {

				if(userUpdated) return res.status(200).send({data: userUpdated});

					else return res.status(404).send({message: 'No se actualizo registro'});

				});	
        }else {
            return res.status(404).send({message: 'El registro no existe'});
        }
	});    
}

module.exports = {
	initProject,
	saveUser,
	loginUser,
	getUser,
	getUsers,
	updateUsers,
	deleteUser,	
	activeUser,
	updatePasword,
	uploadImage,
	getImageFile,
	sendToken,
	updatePasswordForgot,
	singUp
}
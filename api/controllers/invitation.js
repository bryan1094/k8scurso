'use strict'
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs'); 
var path = require('path'); 


var jwt = require('../services/jwt');
const { param } = require('../routes/user');
var Object = require('../models/invitation');
const role = require('../models/role');
const user = require('../models/user');
var randtoken = require('rand-token'); 
var nodemailer = require('nodemailer');



function saveObject(req, res){
	var params = req.body;
	var reg = new Object();

	if(params.email && params.role ){

        var token = randtoken.generate(16);

        reg.email = params.email;
        reg.role = params.role;
		reg.token = token;
        reg.status = 'ACTIVE';
        
        Object.findOne({email: params.email}).exec( (err, object) => {
			if(object){
                return res.status(200).send({message: 'Ya fue enviada una invitacion a este correo'})    
            }else {
                reg.save((err, invitationStored) => {
                    if(err) return res.status(500).send({message: 'Error al guardar registro'});

                        if(invitationStored){
                            return sendEmail(res, req, params.email, token);
                        }

                        else return res.status(404).send({message: 'No se guardo el registro'});
                                
                });
            
            }   
			
		});		
	}else{
		res.status(200).send({
			message: 'Completar campos obligatorios!!'
		});
	}

}


function sendEmail (res, req, correo, token){
    
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'k8scurse@gmail.com',
            pass: 'ks82020@'
        }
    });
    var mailOptions = {
        from: 'k8scurse',
        to: correo,
        subject: 'Token',
        text: 'Has sido invitado a registrarte a la plataforma  k8s para registrarte usa el siguiente Token: ' +  token 
    };
    transporter.sendMail(mailOptions, function(err, info){
        if (err){
            console.log(err);
            res.send(500, err.message);
        } else {
            res.status(200).send({data: req.body});
        }
    });
}


module.exports = {
	saveObject,
}
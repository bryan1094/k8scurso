'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var api = express.Router();//Para tener acceso a los metodos get, post y put
var md_auth = require('../middlewares/authenticated');

var multipart = require('connect-multiparty'); //para subir archivos
var md_upload =multipart({uploadDir: './uploads/users'});//Ruta en la que se guardaran los archivos

api.post('/user', md_auth.ensureAuth, UserController.saveUser); 
api.get('/user/:id', md_auth.ensureAuth, UserController.getUser);
api.get('/users', md_auth.ensureAuth, UserController.getUsers);
api.put('/user/:id', md_auth.ensureAuth, UserController.updateUsers);
api.delete('/user/:id', md_auth.ensureAuth, UserController.deleteUser);
api.post('/user/:id', md_auth.ensureAuth, UserController.activeUser);

api.post('/upload-image-user/:id', [md_auth.ensureAuth, md_upload], UserController.uploadImage);
api.get('/get-image-user/:imageFile',UserController.getImageFile);

api.get('/init',UserController.initProject);


//Servicios Publicos
api.post('/login', UserController.loginUser); 
api.post('/forgot-password', UserController.sendToken);
api.post('/forgot-password-update', UserController.updatePasswordForgot);
api.post('/signup', UserController.singUp); 


api.put('/update-user-pass/:id', md_auth.ensureAuth, UserController.updatePasword);







module.exports = api;
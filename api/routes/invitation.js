'use strict'

var express = require('express');
var objectController = require('../controllers/invitation');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');


api.post('/invitation', md_auth.ensureAuth, objectController.saveObject); 

module.exports = api;
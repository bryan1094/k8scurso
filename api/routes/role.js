'use strict'

var express = require('express');
var ObjectController = require('../controllers/role');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');


api.get('/role', md_auth.ensureAuth, ObjectController.getRoles);
api.post('/role', md_auth.ensureAuth, ObjectController.saveRole); 
api.put('/role/:id', md_auth.ensureAuth, ObjectController.updateRole);
api.delete('/role/:id', md_auth.ensureAuth, ObjectController.deleteRole);
api.post('/role/:id', md_auth.ensureAuth, ObjectController.activeRole); 


module.exports = api;
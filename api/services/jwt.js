'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = '*84J+uMBpj=LD2XE9Ck%=_kWCC+58U!5KMa7?&j!9T+RC^WNTz_DZVjjP@QS^N2xu3*c&#H&c_53G=tRXj8f%UXhUs+CzHMh&d2A*VUwy28D!7j#rJ?8%c!TxFFVFdS8NrV##pL%$NEDN&*j@Lt4$EsK?9gmJqR44xVENTJ=j?QpuF?urG$Gsd2xXLf7_=uEy-KD*Lrh-8HmY68h=cQ8k2WZj!Wnz$dnyKdm=L3Lj9XXGDQLhU*&4GUREYaEEhd?';


exports.createToken = function(user){
	var payload = {
		sub: user._id,
		name: user.name,
		surname: user.surname,
		email: user.email,
		role: user.role,
		img: user.img,
		reset_password: user.reset_password,
		token: user.token,
		status: user.status,
		iat: moment().unix(),
		exp: moment().add(30, 'days').unix
	};

	return jwt.encode(payload, secret )
};
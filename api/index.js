'use strict'

var mongoose = require('mongoose'); 
var app = require('./app');
var port = 3800;



const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/k8s';

mongoose.Promise = global.Promise; 

mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
	.then(() => {
		console.log('Conexion a BD  exito');
		app.listen(port, () => {
			console.log("Servidor corriendo en http://localhost:3800");
		});
	})
	.catch(err => console.log(err));

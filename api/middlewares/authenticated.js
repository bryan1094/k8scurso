'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = '*84J+uMBpj=LD2XE9Ck%=_kWCC+58U!5KMa7?&j!9T+RC^WNTz_DZVjjP@QS^N2xu3*c&#H&c_53G=tRXj8f%UXhUs+CzHMh&d2A*VUwy28D!7j#rJ?8%c!TxFFVFdS8NrV##pL%$NEDN&*j@Lt4$EsK?9gmJqR44xVENTJ=j?QpuF?urG$Gsd2xXLf7_=uEy-KD*Lrh-8HmY68h=cQ8k2WZj!Wnz$dnyKdm=L3Lj9XXGDQLhU*&4GUREYaEEhd?';


exports.ensureAuth = function(req, res, next){
	if (!req.headers.authorization){
		return res.status(403).send({message: 'La peticion no tiene la cabecera de autenticacion'});
	}

	var token = req.headers.authorization.replace(/['"]+/g, '');

	try{
		var payload = jwt.decode(token, secret);

		if(payload.exp <= moment().unix()){
			return res.status(401).send({
				message: 'El token a expirado'
			});
		}
	}catch(exe){
		return res.status(404).send({
				message: 'El token no es valido'
			});

	}

	req.user = payload;
	

	next();
	
}
import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http'; 
import { routing, appRoutingProviders } from './app.routing';
import { MomentModule } from 'angular2-moment';

//Componentes
import { AppComponent } from './app.component';

//Usuarios
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/registers/register.component';
import { HomeComponent } from './components/home/home.component'
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserEditPassComponent } from './components/user-edit-pass/user-edit-pass.component';
import { AllUserComponent } from './components/all-users/all-users.component';

import {InvitationComponent} from './components/invite/invite.component';
import { ForgotComponent } from './components/forgot-pass/forgot.component';
import { ChangeForgotComponent } from './components/change-forgot-pass /change-forgot.component';
import { RoleComponent } from './components/role/role.component';


//Servicios
import {UserService} from './services/user.service'
import { SecurityGuard } from './services/security.guard';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotComponent,
    ChangeForgotComponent,
    HomeComponent,
    UserEditComponent,
    UserEditPassComponent,

    InvitationComponent,
    AllUserComponent,

    RoleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpClientModule,
    MomentModule,
  ],
  providers: [
    appRoutingProviders,
    UserService,
    SecurityGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class Invitation{
    constructor(
        public _id: string,
        public email: string,
        public token: string,
        public role: string,
        public status: String
    ){}
}




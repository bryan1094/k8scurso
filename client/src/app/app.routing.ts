import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/registers/register.component';
import { HomeComponent } from './components/home/home.component'
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { InvitationComponent } from './components/invite/invite.component';
import { ForgotComponent } from './components/forgot-pass/forgot.component';
import { ChangeForgotComponent } from './components/change-forgot-pass /change-forgot.component';
import { RoleComponent } from './components/role/role.component';
import { SecurityGuard } from './services/security.guard';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'home', component: HomeComponent},
    {path: 'login', component: LoginComponent},
    {path: 'forgot', component: ForgotComponent},
    {path: 'forgot-update', component: ChangeForgotComponent},
    {path: 'registro', component: RegisterComponent},
    {path: 'mis-datos', component: UserEditComponent, canActivate:[SecurityGuard]},
    {path: 'invite', component: InvitationComponent, canActivate:[SecurityGuard]},
    {path: 'role', component: RoleComponent, canActivate:[SecurityGuard]},
  
    {path: '**', component: HomeComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
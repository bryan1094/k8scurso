import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global';


@Component({
    selector: 'all-users',
    templateUrl: './all-users.component.html',
    providers: [UserService]
})

export class AllUserComponent implements OnInit{
    public title: string;
    public identity;
    public token;
    public url: string;
    public status: string;
    public page;
    public total;
    public pages;
    public itemsPerPage;
    public users: User[]
    public showImage;
    @Input() companie: string;



    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService
    ){
        this.title = 'Users';
        this.identity = this._userService.getIdentity();
        this.url = GLOBAL.url;
    }

    ngOnInit(){
        this.getUsers();
    }

    getUsers(){
        this._userService.getUsers().subscribe(
            response => {
                console.log(response)
                if(response.users){

                    this.users = response.users
                }else{
                    this.status = 'error'
                }
            },
            error => {
                var errorMessage = <any>error;
                if(errorMessage){
                    this.status = error;
                }
            }
        );
    }
}
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'change-forgot',
    templateUrl: './change-forgot.component.html',
    providers: [UserService]
})

export class ChangeForgotComponent implements OnInit{
    public title:string;
    public user: User;
    public status: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService
    ){
        this.title = 'Cambiar contraseña';
        this.user = new User("","","","","","",true,"","","");
        
    }

    ngOnInit(){
    }

    onSubmit(form){ 
        this._userService.forgotPasswordUpdate(this.user).subscribe(
            response => {
                if(response.data){
                    this.status ='success';
                    form.reset();
                    this._router.navigate(['/login']);
                }else{
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error);
                this.status = 'error';
            }
        );
    }
}
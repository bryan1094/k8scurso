import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { RoleService } from '../../services/role.service';
import { Role } from '../../models/role'

@Component({
    selector: 'roles',
    templateUrl: './role.component.html',
    providers: [UserService, RoleService]
})

export class RoleComponent implements OnInit{
    public title:string;
    public identity:any;
    public token:any;
    public roles: Role[];
    public roleSave: Role;
    public status: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _rolaService: RoleService,
    ){
        this.title = 'Crear';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.roleSave = new Role("","","","");
    }

    ngOnInit(){
        this.getRoles()
    }

   

    getRoles(){ 
        this._rolaService.getRoles(this.token).subscribe(
            response => {
                if(response.data){
                    this.roles = response.data;
                    this.status ='success';
                }else{
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error);
                this.status = 'error';
            }
        );
    }

    onSubmit(form){ 
        this._rolaService.saveRoles(this.roleSave, this.token).subscribe(
            response => {
                if(response.data){
                    this.getRoles();
                    this.status ='success';
                    form.reset()
                }else{
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error);
                this.status = 'error';
            }
        );
    }

    deleteRole(id){ 
        this._rolaService.deleteRole(id, this.token).subscribe(
            response => {
                if(response.data){
                    this.getRoles();
                    this.status ='success';
                }else{
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error);
                this.status = 'error';
            }
        );
    }
}
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { InvitationService } from '../../services/invitation.service'
import { RoleService } from '../../services/role.service';
import { Role } from '../../models/role'
import { Invitation } from '../../models/inivitation'

@Component({
    selector: 'invitation',
    templateUrl: './invite.component.html',
    providers: [UserService, InvitationService, RoleService]
})

export class InvitationComponent implements OnInit{
    public title:string;
    public identity:any;
    public token:any;
    public roles: Role[];
    public invitation: Invitation;
    public status: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _rolaService: RoleService,
        private _invitationService: InvitationService
    ){
        this.title = 'Invitar';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.invitation = new Invitation("","","","","");
        
    }

    ngOnInit(){
        this.getRole();
    }

    getRole(){
        this._rolaService.getRoles(this.token).subscribe(
            response => {
                if(response.data){
                    this.roles = response.data;
                }else{
                    console.log('No hay registros para mostrar');
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    onSubmit(form){ 
        this._invitationService.sendInvitation(this.invitation, this.token).subscribe(
            response => {
                if(response.data){
                    this.status ='success';
                    form.reset();
                }else{
                    this.status = 'error';
                }
            },
            error => {
                console.log(<any>error);
            }
        );
    }
}
import { Component, OnInit } from '@angular/core';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';


@Component({
    selector:'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers: [UserService]

})
export class HomeComponent implements OnInit{
    public title:string;
    public identity;
    public counter;

    constructor( 
        private _userService: UserService
    ){
        this.title = 'Bienvenido al curso k8s';
        this.identity = _userService.getIdentity();
    }

    ngOnInit(){
        this.initProject()
    }

    initProject(){
        this._userService.initProject().subscribe(
            response => {
                console.log(response)
                if(response){
                }else{
                }
            },
            error => {
                var errorMessage = <any>error;
                if(errorMessage){
                }
            }
        );
    }
}
import {Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';
import { Role } from '../models/role';

@Injectable()
export class RoleService{
    public url:string;
    public identity:any;
    public token:any;
 

    constructor(public _http: HttpClient){
        this.url = GLOBAL.url;
    }


    getRoles(token:any):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization',token);

                                       
        return this._http.get(this.url+'role', {headers: headers});
    }

    saveRoles(role:any, token:any):Observable<any>{
        let params = JSON.stringify(role);
        
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization',token);

                                       
        return this._http.post(this.url+'role',params, {headers: headers});
    }

    deleteRole(id:any, token:any):Observable<any>{
    
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization',token);

                                       
        return this._http.delete(this.url+'role/'+id, {headers: headers});
    }
}
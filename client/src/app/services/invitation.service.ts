import {Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Invitation } from '../models/inivitation';

@Injectable()
export class InvitationService{
    public url:string;
    public identity;
    public token;
    public stats;

    constructor(public _http: HttpClient){
        this.url = GLOBAL.url;
    }

    sendInvitation(invitation:any,token:any ): Observable<any>{
        
        let params = JSON.stringify(invitation);
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                       .set('Authorization',token);

        return this._http.post(this.url+'invitation',params, {headers:headers});
    }

    
}